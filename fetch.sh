#!/bin/sh

d=linux
if [ -d $d ]; then
	cd $d
	git pull origin master
	exit 0
fi
git clone --depth 1 git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
