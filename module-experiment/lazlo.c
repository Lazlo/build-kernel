#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Daniel Laszlo Sitzer");
MODULE_DESCRIPTION("Exploratory kernel module");
MODULE_VERSION("0.1");

static int __init lazlo_init(void)
{
	printk(KERN_INFO "lazlo: Hello\n");
	return 0;
}

static void __exit lazlo_exit(void)
{
	printk(KERN_INFO "lazlo: Exit\n");
}

module_init(lazlo_init);
module_exit(lazlo_exit);
