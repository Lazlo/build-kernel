@Library('pipeline-library') _

pipeline {
    agent any
    environment {
        APT_LOCK       = "apt-lock-${env.NODE_NAME}"
        SRC_DIR        = "linux"
    }
    stages {
        stage('Setup') {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    retry(5) {
                        lock("${env.APT_LOCK}") {
                            sh 'sudo ./setup.sh'
                        }
                    }
                }
            }
        }
        stage('Fetch') {
            steps {
                sh './fetch.sh'
                sh "du -hs ${env.SRC_DIR}"
            }
        }
        stage('Clean') {
            steps {
                dir("${env.SRC_DIR}") {
                    sh 'git clean -dfx'
                }
            }
        }
        stage('Configure') {
            steps {
                dir("${env.SRC_DIR}") {
                    sh 'make defconfig'
                    sh 'sed -i -E \'s/(CONFIG_SYSTEM_TRUSTED_KEYS)=.*/\\1=""/\' .config'
                }
            }
        }
        stage('Build') {
            steps {
                dir("${env.SRC_DIR}") {
                    sh 'make -j$(nproc) CC=/usr/lib/ccache/gcc deb-pkg'
                }
                archiveArtifacts artifacts: "*.deb"
            }
        }
    }
    post {
        always {
            commonStepNotification()
        }
    }
}
